#! /usr/bin/bash
ZIP=sciter-sdk.zip
SDK_URL="https://sciter.com/sdk/$ZIP"
SDK_DIR="sciter-sdk"

[ ! -d $SDK_DIR ] && mkdir -p $SDK_DIR

pushd $SDK_DIR
wget $SDK_URL
unzip $ZIP
rm $ZIP
popd
