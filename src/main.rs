use std::env;
use std::cell::Cell;
use std::sync::RwLock;
use std::rc::Rc;

use glfw::{Action, Context, Key, WindowHint, WindowEvent, SwapInterval};
use anyhow::Result;
use sciter::{self, RuntimeOptions, HostHandler};
use thiserror::Error;


#[derive(Error, Debug)]
enum SciterError {
  #[error("Sciter returned an error: {0}")]
  ErrMessage(String),
  #[error("Sciter set_option was unsuccessful.")]
  SetOptionError(())
}

#[derive(Clone)]
struct WindowlessHandler {
  redraw_needed: Rc<RwLock<bool>>
}

impl HostHandler for WindowlessHandler {
  fn on_invalidate(&mut self, _pnm: &sciter::host::SCN_INVALIDATE_RECT) {
    let mut rn = self.redraw_needed.write().unwrap();
    *rn = true;
  }
}

fn error_callback(_: glfw::Error, description: String, error_count: &Cell<usize>) {
  println!("GLFW error {}: {}", error_count.get(), description);
  error_count.set(error_count.get() + 1);
}

fn main() -> Result<()> {
  let cwd = env::current_dir()?;

  let lib_path = format!("{}/sciter-sdk/bin.lnx/x64lite/libsciter.so", cwd.to_str().unwrap());
  sciter::set_library(&lib_path).map_err(SciterError::ErrMessage)?;
  sciter::set_options(RuntimeOptions::DebugMode(true)).map_err(SciterError::SetOptionError)?;
  sciter::set_options(RuntimeOptions::UxTheming(false)).map_err(SciterError::SetOptionError)?;

  let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

  glfw.set_error_callback(Some(
    glfw::Callback {
        f: error_callback,
        data: Cell::new(0),
    }
  ));

  glfw.window_hint(WindowHint::ContextVersion(3, 0));

  let (mut window, events) = glfw
    .create_window(300, 300, "Hello this is window", glfw::WindowMode::Windowed)
    .expect("Failed to create GLFW window.");

  use sciter::windowless::{Message, handle_message};
	let scwnd = { &window as *const _ as sciter::types::HWINDOW };
  handle_message(scwnd, Message::Create { backend: sciter::types::GFX_LAYER::SKIA_OPENGL, transparent: false, });

  let (x_scale, _) = window.get_content_scale();
  handle_message(scwnd, Message::Resolution { ppi: 96 * x_scale as u32 });

  let handler = WindowlessHandler { redraw_needed: Rc::new(RwLock::new(true)) };
  let instance = sciter::Host::attach_with(scwnd, handler.clone());

  window.make_current();

  gl::load_with(|s| glfw.get_proc_address_raw(s) as *const std::os::raw::c_void);
  glfw.set_swap_interval(SwapInterval::Sync(1));

  window.set_key_polling(true);

  let html = include_bytes!("minimal.html");
  instance.load_html(html, Some("example://minimal.html"));

  while !window.should_close() {
    glfw.poll_events();
    let (mut width, mut height) = window.get_framebuffer_size();
    handle_message(scwnd, Message::Size { width: width as u32, height: height as u32 });

    let ticks = glfw.get_time() * 1000.0;
    handle_message(scwnd, Message::Heartbit { milliseconds: ticks as u32 });

    for (_, event) in glfw::flush_messages(&events) {
      println!("{:?}", event);

      match event {
        WindowEvent::Size(w, h) => {
          width = w;
          height = h;
          handle_message(scwnd, Message::Size { width: width as u32, height: height as u32 });
        },
        WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
          window.set_should_close(true)
        },
        _ => {}
      }
    }

    if *(handler.redraw_needed.read().unwrap()) {
      unsafe {
        gl::Viewport(0, 0, width, height);
        gl::ClearColor(0.3, 0.3, 0.5, 1.0);
        gl::Clear(gl::COLOR_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
      }

      handle_message(scwnd, Message::Redraw);
      window.swap_buffers();
      let mut rn = handler.redraw_needed.write().unwrap();
      *rn = false;
    }
  }

  Ok(())
}
